<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//access the authenticated user via the Auth facade
use Illuminate\Support\Facades\Auth;

use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{

    //Controller is for the user logout
    public function logout(){
        // it will logout the currently logged in user.
        Auth::logout();
        return redirect('/login');
    }

    //action to return a view containing a form for a blog post creation
    public function createPost(){
        return view('posts.create');
    }

    public function savePost(Request $request){
        //to check whether there is an authenticated user or none
        if(Auth::user()){
            //instantiate a new Post Oject from the Post method and then save it in $post variable:
            $post = new Post;

            //define the properties of the $post object using the received form data
            $post->title=$request->input('title');
            $post->body=$request->input('content');

            //this will get the id of the authenticated user and set it as the foreign jet user_id of the new post.
            $post->user_id=(Auth::user()->id);

            //save the post object in our Post Table;
            $post->save();

            return redirect('/posts');
        }else{
            return redirect('/login');
        }
    }

    //controller will return all the the blog posts
    public function showPosts(){
        //the all() method will save all the records in our Post Table in our $post variable
        $posts = Post::all()->WHERE('isActive', 1);

        return view('posts.showPosts')->with('posts', $posts);
    }

    public function welcomePage(){
        $posts = Post::inRandomOrder()->WHERE('isActive', 1)->limit(3)->get();

        return view('welcome')->with('posts', $posts);
    }

    public function myPosts(){
        if(Auth::user()){
            
            $posts = Auth::user()->posts;

            return view('posts.showPosts')->with('posts', $posts);
        }else{
            return redirect('/login');
        }
    }

    public function showSinglePost($id){
        $posts = Post::find($id);
        $comments = PostComment::all()->WHERE('post_id', $id);
        
        return view('posts.showSingle')->with('posts', $posts)->with('comments', $comments);
    }

    public function toUpdatePost($id){
        $postToEdit = Post::find($id);
        if(Auth::user()){
            return view('posts.edit')->with('posts', $postToEdit);
        } else {
            return redirect('/login');
        }
        
    }

    public function updatePost(Request $request, $id){
        $posts = Post::find($id);
        $posts->title = $request->input('title');
        $posts->body = $request->input('body');
        if(Auth::user()){
            $posts->update();

            return redirect()->back()->with('message', 'Post have been updated');
        } else {
            return redirect('/login');
        }
    }

    public function archivePost($id){
        $posts = Post::find($id);
        $posts->isActive = 0;

        if(Auth::user()){
            $posts->update();
            return redirect()->back()->with('message', 'Post have been deleted');
        } else {
            return redirect('/login');
        }
    }

    public function like($id){
        $post = Post::find($id);

        if($post->user_id != Auth::user()->id){

            if($post->likes->contains("user_id", Auth::user()->id)){
                //delete the like record made by the user before
                PostLike::where('post_id', $post->id)->where('user_id', Auth::user()->id)->delete();
            }else{
                //create a new like record to like this post
                //instantiate a new PostLike object from the Postlike model
                $postlike = new PostLike;
                //define the properties of the $postlike
                $postlike->post_id = $post->id;
                $postlike->user_id = Auth::user()->id;

                //save this postlike object in the database
                $postlike->save();
            }
        }

        return redirect("/posts/$id");
    }

    public function comment(Request $request, $id){
        $post = Post::find($id);
        $postComment = new PostComment;

        if(Auth::user()){
            $postComment->user_id = Auth::user()->id;
            $postComment->post_id = $id;
            $postComment->content = $request->input('content');
            $postComment->save();

            return redirect()->back();
        } else {
            return redirect('/login');
        }
    }

    public function showComments($id){
        
    }
}