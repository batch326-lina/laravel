<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PostController::class, 'welcomePage']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//This route is for logging out.
Route::get('/logout', [PostController::class, 'logout']);


//This route is for creation of a new post:
Route::get('/posts/create', [PostController::class, 'createPost']);

//This route is for saving the post on our database:
Route::post('/posts', [PostController::class, 'savePost']);


//This route is for the list of post on our database:
Route::get('/posts', [PostController::class, 'showPosts']);

Route::get('/myPosts', [PostController::class, 'myPosts']);

Route::get('/posts/{id}', [PostController::class, 'showSinglePost']);

Route::get('/posts/{id}/edit', [PostController::class, 'toUpdatePost']);

Route::post('/posts/{id}', [PostController::class, 'updatePost']);

Route::post('/posts/{id}/delete', [PostController::class, 'archivePost']);

Route::put('/posts/{id}/like', [PostController::class, 'like']);

Route::post('/posts/{id}/comment', [PostController::class, 'comment']);