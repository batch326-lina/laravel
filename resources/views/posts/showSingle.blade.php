@extends('layouts.app')

@section('content')
	<div class = "card col-6 mx-auto">
		<div class = 'card-body'>
			<h2 class = 'card-title'>{{$posts->title}}</h2>
			<p class = "card-subtitle text-muted">Author: {{$posts->user->name}}</p>
			<p class = 'card-subtitle text-muted mb-3'>Created at: {{$posts->created_at}}</p>
			<h4>Content:</h4>
			<p class = "card-text">{{$posts->body}}</p>

			

			@if(Auth::user())
			@if(Auth::id() != $posts->user_id)
				<form class="d-inline" method="POST" action="/posts/{{$posts->id}}/like">
					@csrf
					@method('PUT')
					@if($posts->likes->contains('user_id', Auth::id()))
					<button type="submit" class="btn btn-danger">Unlike</button>
					@else
					<button type="submit" class="btn btn-success">Like</button>
				</form>
				@endif
			@endif
			<button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">Comment</button>
			@endif
			<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="staticBackdropLabel">Add comment here</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
	  	<form method="POST" id="comment" action="/posts/{{$posts->id}}/comment">
			@csrf
			<textarea rows="4" cols="50" name="content" style="resize: none;"></textarea>
			
		</form>
      </div>
	  <div class="modal-footer">
	  <button type="submit" form="comment" class="btn btn-primary">Submit</button>
		  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
			<a href="/posts" class = "btn btn-info">View all posts</a>
		</div>		
	</div>

	
<div class="container-fluid">
	<h1>Comments:</h1>
@foreach($comments as $comments)
<div class = "card col-6 mx-auto">
		<div class = 'card-body'>
			<h2 class = 'card-title'>{{$comments->content}}</h2>
			<p class = "card-subtitle text-muted ms-auto">Posted By: {{$comments->user->name}}</p>
			<p class = 'card-subtitle text-muted ms-auto'>Posted on: {{$comments->created_at}}</p>
		</div>
	</div>
@endforeach
</div>



@endsection	