@extends('layouts.app')

@section('content')
	<div class='container'>
        <img class="img-fluid" style="width: 50%; height:50%; display: block; margin-left: auto; margin-right: auto;" src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png" alt="laravel logo">
    </div>

    <h3 class='text-center'>Featured Posts:</h3>
    @if(count($posts)>0)
        @foreach($posts as $post)
            <div class='card text-center mx-auto mt-2'>
                <div class='card-body'>
                    <h4 class='card-title'><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                    <p>Author: {{$post->user->name}}</p>
                </div>
            </div>
        @endforeach
    @endif
@endsection 